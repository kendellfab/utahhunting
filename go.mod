module gitlab.com/kendellfab/utahhunting

go 1.13

require (
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/gorilla/sessions v1.2.0
	github.com/jinzhu/gorm v1.9.12
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/magefile/mage v1.9.0
	gitlab.com/kendellfab/fazer v0.10.3
	gitlab.com/kendellfab/gochartjs v0.1.0
	gonum.org/v1/plot v0.0.0-20200111075622-4abb28f724d5 // indirect
)

replace gitlab.com/kendellfab/gochartjs => ../gochartjs
