Vue.mixin({
    delimiters: ['[[', ']]'],
    methods: {
        formatTime: function(t) {
            var ft = moment(t);
            return ft.format("MM/DD/YYYY");
        },
        formatDuration: function(seconds) {
            var date = new Date(null);
            date.setSeconds(seconds);
            return date.toISOString().substr(11, 8);
        }
    }
});