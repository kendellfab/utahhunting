document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('.message .message-header .delete').forEach(function(elem) {
        var header = elem.parentNode;
        var message = header.parentNode;
        elem.addEventListener('click', function(evt) {
            message.parentNode.removeChild(message);
        });
    });

    var burgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    if(burgers.length > 0) {
        burgers.forEach(function(elem) {
            elem.addEventListener('click', function() {
                var t = elem.dataset.target;
                var target = document.getElementById(t);
                elem.classList.toggle('is-active');
                target.classList.toggle('is-active');
            })
        })
    }
});

