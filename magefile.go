// +build mage

package main

import (
	"fmt"
	"os/exec"

	"github.com/magefile/mage/sh"
)

// Default target to run when none is specified
// If not set, running mage will list available targets
// var Default = Build

// A build step that requires additional params, or platform specific steps for example
/*func Build() error {
	mg.Deps(InstallDeps)
	fmt.Println("Building...")
	cmd := exec.Command("go", "build", "-o", "MyApp", ".")
	return cmd.Run()
}
*/
// A custom install step if you need your bin someplace other than go/bin
/*func Install() error {
	mg.Deps(Build)
	fmt.Println("Installing...")
	return os.Rename("./MyApp", "/usr/bin/MyApp")
}*/

// Manage your deps, or running package managers.
/*func InstallDeps() error {
	fmt.Println("Installing Deps...")
	cmd := exec.Command("go", "get", "github.com/stretchr/piglatin")
	return cmd.Run()
}*/

// Clean up after yourself
/*func Clean() {
	fmt.Println("Cleaning...")
	os.RemoveAll("MyApp")
}*/

var remoteCommand = sh.RunCmd("ssh", "-t", "kendellfab@cloud.kendellfabrici.us")
var binaryName = "utahhuntresultsd"
var serviceName = "utahhuntresults"

func Build() {
	cmd := exec.Command("go", "build", "-o", binaryName, "cmd/utahhuntingd/main.go")
	cmd.Run()
}

func Clean() {
	cmd := exec.Command("rm", binaryName)
	cmd.Run()
}

func DeployBinary() {
	sh.Run("scp", binaryName, "kendellfab@cloud.kendellfabrici.us:/var/www/utahhuntresults")
}

func Sync() error {
	return sh.Run("rsync", "--delete", "-pthrvz", "static", "kendellfab@cloud.kendellfabrici.us:/var/www/utahhuntresults")
}

func Data() {
	sh.Run("scp", "utahhunts.db", "kendellfab@cloud.kendellfabrici.us:/var/www/utahhuntresults")
}

func Stop() {
	remoteCommand("sudo", "/bin/systemctl", "stop", serviceName)
}

func Start() {
	remoteCommand("sudo", "/bin/systemctl", "start", serviceName)
}

func ImportGEDeer() error {
	data := map[string]string{"2019": "data/2019GeDeer.csv",
		"2018": "data/2018GeDeer.csv",
		"2017": "data/2017GeDeer.csv",
		"2016": "data/2016GeDeer.csv",
	}

	for year, file := range data {
		fmt.Println("Importing ", year, "From", file)
		cmd := exec.Command("go", "run", "cmd/imports/main.go", "-i", file, "-y", year)
		err := cmd.Run()
		if err != nil {
			return err
		}
	}

	return nil
}
