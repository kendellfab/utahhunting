package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	f, err := os.Open("data/2018GeDeer.csv")
	if err != nil {
		log.Fatal("error opening file", err)
	}
	defer f.Close()

	uniqueHuntNames := make(map[string]bool)
	cr := csv.NewReader(f)
	for {
		records, err := cr.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal("error loading hunts", err)
		}

		uniqueHuntNames[records[1]] = true
	}

	count := 1
	for key, _ := range uniqueHuntNames {
		fmt.Println(count, key)
		count++
	}

}
