package main

import (
	"log"
	"os"

	"gonum.org/v1/plot/plotutil"

	"gitlab.com/kendellfab/utahhunting/internal/platform/hunt"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

func main() {

	f, err := os.Open("data/2018GeDeer.csv")
	if err != nil {
		log.Fatal("error opening file", err)
	}
	defer f.Close()

	repo := hunt.InitRepo(f, 2018, "General Entry")
	hunts, _ := repo.HuntsByTypeName()
	outputType(hunts, repo.HuntsNameSet())
}

func outputType(hunts map[string]map[string]hunt.Hunt, names map[string]hunt.Name) {
	p, err := plot.New()
	p.Title.Text = "2018 Deer Hunt"
	p.Y.Label.Text = "Success Rate"
	p.X.Label.Text = "Hunt Name"
	if err != nil {
		log.Fatal("error creating plot", err)
	}

	var nominalX []string
	for k, _ := range names {
		nominalX = append(nominalX, k)
	}

	var parts []interface{}
	for k, hs := range hunts {
		var vals plotter.XYs

		count := 0
		for k, _ := range names {
			if h, ok := hs[k]; ok {
				vals = append(vals, plotter.XY{X: float64(count), Y: h.SuccessRate})
			} else {
				vals = append(vals, plotter.XY{X: float64(count), Y: 0})
			}

			count++
		}
		parts = append(parts, k)
		parts = append(parts, vals)
	}

	plotutil.AddLines(p, parts...)

	p.NominalX(nominalX...)
	if err := p.Save(60*vg.Inch, 10*vg.Inch, "deer-ge-2018.png"); err != nil {
		log.Fatal("error saving plot", err)
	}
}
