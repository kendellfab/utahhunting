package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/kendellfab/utahhunting/internal/platform/config"
	"gitlab.com/kendellfab/utahhunting/internal/platform/hunt"
)

func main() {

	var importFile string
	var huntYear int
	var entryType string
	var species string
	var dbPath string
	var useLocal bool
	flag.StringVar(&importFile, "i", "data/2018GeDeer.csv", "Set the path to the import file.")
	flag.IntVar(&huntYear, "y", 2018, "Set the year of the hunt report.")
	flag.StringVar(&entryType, "e", hunt.GeneralEntry, "Set the season type: GeneralEntry or LimitedEntry")
	flag.StringVar(&species, "s", hunt.SpeciesDeer, "Set the species type: Deer, Elk, ...")
	flag.StringVar(&dbPath, "d", "utahhunts.db", "Set the database file path.")
	flag.BoolVar(&useLocal, "l", false, "Set to use sqlite")
	flag.Parse()

	f, err := os.Open(importFile)
	if err != nil {
		log.Fatal("error opening file", err)
	}
	defer f.Close()

	var db *gorm.DB
	if(useLocal) {
		db, err = gorm.Open("sqlite3", dbPath)
	} else {
		dbStr := fmt.Sprintf(config.PostgresString, "uhr", "uhr", "uhr")
		log.Println(dbStr)
		db, err = gorm.Open("postgres", dbStr)
	}

	if err != nil {
		log.Fatal("error opening database file", err)
	}
	defer db.Close()

	manager := hunt.NewManager(db)

	gr := hunt.InitRepo(f, huntYear, entryType, species)
	hunts := gr.ListHunts()

	for _, h := range hunts {
		err = manager.StoreHunt(&h)
		if err != nil {
			log.Println("error storing hunt:", h.HuntNumber, h.HuntYear, h.HuntName)
		}
	}
}
