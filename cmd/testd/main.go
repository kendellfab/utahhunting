package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.com/kendellfab/utahhunting/internal/platform/config"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/kendellfab/fazer"
	"gitlab.com/kendellfab/utahhunting/internal/platform/hunt"
	"gitlab.com/kendellfab/utahhunting/internal/platform/web"
)

func main() {

	var dbPath string
	flag.StringVar(&dbPath, "d", "utahhunts.db", "Set the database file path.")
	flag.Parse()

	var c config.Config
	err := envconfig.Process("utahhunting", &c)
	if err != nil {
		log.Fatal("error processing config", err)
	}

	manifest := fazer.MustDecodeManifestJson(c.ManifestPath, fazer.SetProduction(c.Prod), fazer.AssetsPath(c.AssetsPath))
	store := sessions.NewCookieStore([]byte(c.SessionKey), []byte(c.SessionEncryption))
	fzr := fazer.NewFazer(store, "static/tpls", c, c.Prod)
	fzr.AddManifest(manifest)

	db, err := gorm.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatal("error opening database file", err)
	}
	defer db.Close()
	db.LogMode(true)

	manager := hunt.NewManager(db)

	mux := chi.NewRouter()
	mux.Use(middleware.RealIP)
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)

	h := &web.Handler{Fazer: fzr, Manifest: manifest, Store: store}
	t := web.Test{Handler: h, Manager: manager}

	mux.Get("/", t.GetRoot)
	mux.Get("/api/type/name/{year:[0-9]+}/{region}", t.GetTypeName)
	mux.Get("/api/old/{year:[0-9]+}", t.GetWithoutRegion)

	mux.Handle("/*", http.FileServer(http.Dir("static/dist")))
	srv := &http.Server{Addr: ":3000",
		Handler: mux,
	}
	if srvErr := srv.ListenAndServe(); srvErr != nil {
		log.Fatal("Server error:", srvErr)
	}
}
