package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/kendellfab/utahhunting/internal/api"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/kendellfab/fazer"
	"gitlab.com/kendellfab/utahhunting/internal/front"
	"gitlab.com/kendellfab/utahhunting/internal/platform/config"
	"gitlab.com/kendellfab/utahhunting/internal/platform/hunt"
	"gitlab.com/kendellfab/utahhunting/internal/platform/web"
)

func main() {
	var c config.Config
	err := envconfig.Process("utahhunting", &c)
	if err != nil {
		log.Fatal("error processing config", err)
	}

	db, err := gorm.Open("postgres", fmt.Sprintf(config.PostgresString, c.DbUser, c.DbPass, c.DbDB))
	if err != nil {
		log.Fatal("error opening database", err)
	}
	defer db.Close()
	db.LogMode(!c.Prod)
	manager := hunt.NewManager(db)

	manifest := fazer.MustDecodeManifestJson(c.ManifestPath, fazer.SetProduction(c.Prod), fazer.AssetsPath(c.AssetsPath))
	store := sessions.NewCookieStore([]byte(c.SessionKey), []byte(c.SessionEncryption))
	fzr := fazer.NewFazer(store, c.TplsPath, c, c.Prod)
	fzr.AddManifest(manifest)
	rand.Seed(time.Now().Unix())
	fzr.RegisterMerge(func(r *http.Request, data map[string]interface{}) {
		heros := []string{"buck-sunset", "grazing-bulls"}
		idx := rand.Intn(len(heros))
		data["hero"] = heros[idx]
		data["hero"] = "buck-sunset"

		if species, err := manager.UniqueSpecies(); err == nil {
			data["Species"] = species
		} else {
			log.Println("error loading species", err)
		}

	})

	mux := chi.NewRouter()
	mux.Use(middleware.RealIP)
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)

	h := &web.Handler{
		Fazer:    fzr,
		Manifest: manifest,
		Store:    store,
	}

	f := &front.Front{Handler: h, Manager: &manager}
	apiTypeName := api.NewTypeName(h, manager)

	mux.Get("/", f.GetHome)
	mux.Get("/species/{specie}", f.GetSpecies)

	api := chi.NewRouter()
	api.Get("/type/{entry}/name/{year:[0-9]+}/{region}", apiTypeName.GetTypeName)

	mux.Mount("/api", api)

	mux.Handle("/*", http.FileServer(http.Dir(c.AssetsPath)))
	srv := &http.Server{Addr: fmt.Sprintf(":%d", c.Port), Handler: mux}
	if srvErr := srv.ListenAndServe(); srvErr != nil {
		log.Fatal("server error", srvErr)
	}

}
