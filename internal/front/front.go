package front

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"

	"gitlab.com/kendellfab/utahhunting/internal/platform/hunt"
	"gitlab.com/kendellfab/utahhunting/internal/platform/web"
)

type Front struct {
	*web.Handler
	*hunt.Manager
}

func (f *Front) GetHome(w http.ResponseWriter, r *http.Request) {
	f.RenderTemplate(w, r, nil, "front.home")
}

func (f *Front) GetSpecies(w http.ResponseWriter, r *http.Request) {
	// Using a fake singular form of species, as the merge adds species to the pipeline for the navigation
	data := make(map[string]interface{})
	specie := chi.URLParam(r, "specie")
	data["Specie"] = specie

	if entryTypes, err := f.Manager.UniqueEntryTypesForSpecies(specie); err == nil {
		data["EntryTypes"] = entryTypes
	} else {
		log.Println("error loading entry types", err)
	}

	f.RenderTemplate(w, r, data, "front.species")
}
