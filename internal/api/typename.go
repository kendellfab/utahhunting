package api

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/kendellfab/gochartjs"
	"gitlab.com/kendellfab/utahhunting/internal/platform/hunt"
	"gitlab.com/kendellfab/utahhunting/internal/platform/web"
)

type TypeName struct {
	*web.Handler
	hunt.Manager
}

func NewTypeName(h *web.Handler, m hunt.Manager) *TypeName {
	return &TypeName{Handler: h, Manager: m}
}

func (tn *TypeName) GetTypeName(w http.ResponseWriter, r *http.Request) {
	year, _ := strconv.Atoi(chi.URLParam(r, "year"))
	reg := chi.URLParam(r, "region")
	et := chi.URLParam(r, "entry")

	hunts, _ := tn.Manager.HuntsByTypeName(year, reg, et, hunt.SpeciesDeer)
	nameSet, _ := tn.Manager.HuntsNameSetInRegion(year, reg, et, hunt.SpeciesDeer)

	chart := gochartjs.NewChart(fmt.Sprintf("%d Deer Hunt -- %s", year, reg))
	chart.AddLabels(nameSet).AddXAxes("Hunt Names", false).AddYAxes("Success Rate", true)

	huntTypes, typeErr := tn.Manager.HuntsTypeSetInRegion(year, reg, hunt.GeneralEntry, hunt.SpeciesDeer)
	if typeErr != nil {
		log.Println("error loading hunt types", typeErr)
		http.Error(w, typeErr.Error(), http.StatusInternalServerError)
		return
	}

	for _, ht := range huntTypes {
		hs := hunts[ht]
		var vals []interface{}
		for _, n := range nameSet {
			if h, ok := hs[n]; ok {
				vals = append(vals, h.SuccessRate)
			} else {
				vals = append(vals, 0)
			}
		}
		chart.AddDatum(gochartjs.ChartDatum{
			Label:           ht,
			BackgroundColor: "",
			BorderColor:     "",
			Data:            vals,
			Fill:            false,
		})
	}

	chart.Options.Legend.Position = "top"

	tn.RenderJson(w, r, chart)
}
