package config

const (
	ConnectionString = "%s:%s@/%s?charset=utf8&parseTime=True&loc=Local"
	PostgresString   = "user=%s password=%s dbname=%s sslmode=disable"
)

type Config struct {
	Prod              bool
	SessionKey        string
	SessionEncryption string
	Port              int    `default:"3000"`
	ManifestPath      string `default:"static/manifest.json"`
	TplsPath          string `default:"static/tpls"`
	AssetsPath        string `default:"static/dist"`
	DbPath            string `default:"utahhunts.db"`
	DbUser            string `default:"uhr"`
	DbDB              string `default:"uhr"`
	DbPass            string `default:"uhr"`
}
