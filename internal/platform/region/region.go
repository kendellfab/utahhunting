package region

const (
	Northern = "Northern"
	Central  = "Central"
	Southern = "Southern"
	Unknown  = "Unknown"
)

var RegionMap = map[string]string{
	"Beaver":                         Southern,
	"Box Elder":                      Northern,
	"Cache":                          Northern,
	"Central Mtns, Manti/San Rafael": Central,
	"Central Mtns, Nebo":             Central,
	"Chalk Creek/East Canyon/Morgan-South Rich": Northern,
	"Fillmore":                     Central,
	"Kamas":                        Northern,
	"La Sal, La Sal Mtns":          Central,
	"Monroe":                       Southern,
	"Mt Dutton":                    Southern,
	"Nine Mile":                    Central,
	"North Slope":                  Northern,
	"Ogden":                        Northern,
	"Oquirrh-Stansbury":            Northern,
	"Panguitch Lake":               Southern,
	"Pine Valley":                  Southern,
	"Plateau, Boulder/Kaiparowits": Southern,
	"Plateau, Fishlake":            Southern,
	"Plateau, Thousand Lakes":      Southern,
	"San Juan, Abajo":              Southern,
	"South Slope, Bonanza/Vernal":  Central,
	"South Slope, Yellowstone":     Central,
	"Southwest Desert":             Central,
	"Wasatch Mtns, East":           Central,
	"Wasatch Mtns, West":           Northern,
	"West Desert, Tintic":          Central,
	"West Desert, West":            Central,
	"Zion":                         Southern,
}

func GetRegion(huntName string) string {
	if r, ok := RegionMap[huntName]; ok {
		return r
	}
	return Unknown
}
