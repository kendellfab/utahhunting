package web

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/kendellfab/gochartjs"
	"gitlab.com/kendellfab/utahhunting/internal/platform/hunt"
)

type Test struct {
	*Handler
	hunt.Manager
}

func (t Test) GetRoot(w http.ResponseWriter, r *http.Request) {
	year := r.URL.Query().Get("year")
	if year == "" {
		year = "2018"
	}
	data := map[string]interface{}{"year": year}
	t.RenderTemplate(w, r, data, "test.root")
}

func (t Test) GetWithoutRegion(w http.ResponseWriter, r *http.Request) {
	year, _ := strconv.Atoi(chi.URLParam(r, "year"))
	hunts, err := t.Manager.HuntsByType(year, hunt.GeneralEntry, hunt.SpeciesDeer)
	nameSet, nsErr := t.Manager.HuntsNameSetYear(year, hunt.GeneralEntry, hunt.SpeciesDeer)
	if err != nil || nsErr != nil {
		log.Println("error loading hunts for year", err, nsErr)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	chart := gochartjs.NewChart(fmt.Sprintf("%d Deer Hunt", year))
	chart.AddLabels(nameSet).AddXAxes("Hunt Names", false).AddYAxes("Success Rate", true)

	huntTypes, typeErr := t.Manager.HuntsTypeSetYear(year, hunt.GeneralEntry, hunt.SpeciesDeer)
	if typeErr != nil {
		log.Println("error loading hunt types", typeErr)
		http.Error(w, typeErr.Error(), http.StatusInternalServerError)
		return
	}

	for _, ht := range huntTypes {
		hs := hunts[ht]
		var vals []interface{}
		for _, n := range nameSet {
			if h, ok := hs[n]; ok {
				vals = append(vals, h.SuccessRate)
			} else {
				vals = append(vals, 0)
			}
		}
		chart.AddDatum(gochartjs.ChartDatum{
			Label:           ht,
			BackgroundColor: "",
			BorderColor:     "",
			Data:            vals,
			Fill:            false,
		})
	}

	t.RenderJson(w, r, chart)
}

func (t Test) GetTypeName(w http.ResponseWriter, r *http.Request) {
	year, _ := strconv.Atoi(chi.URLParam(r, "year"))
	reg := chi.URLParam(r, "region")

	hunts, err := t.Manager.HuntsByTypeName(year, reg, hunt.GeneralEntry, hunt.SpeciesDeer)
	nameSet, nameSetErr := t.Manager.HuntsNameSetInRegion(year, reg, hunt.GeneralEntry, hunt.SpeciesDeer)
	if err != nil || nameSetErr != nil {
		log.Println("error loading hunts by type and name", err, nameSetErr)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	chart := gochartjs.NewChart(fmt.Sprintf("%d Deer Hunt -- %s", year, reg))
	chart.AddLabels(nameSet).AddXAxes("Hunt Names", false).AddYAxes("Success Rate", true)

	huntTypes, typeErr := t.Manager.HuntsTypeSetInRegion(year, reg, hunt.GeneralEntry, hunt.SpeciesDeer)
	if typeErr != nil {
		log.Println("error loading hunt types", typeErr)
		http.Error(w, typeErr.Error(), http.StatusInternalServerError)
		return
	}

	for _, ht := range huntTypes {
		hs := hunts[ht]
		var vals []interface{}
		for _, n := range nameSet {
			if h, ok := hs[n]; ok {
				vals = append(vals, h.SuccessRate)
			} else {
				vals = append(vals, 0)
			}
		}
		chart.AddDatum(gochartjs.ChartDatum{
			Label:           ht,
			BackgroundColor: "",
			BorderColor:     "",
			Data:            vals,
			Fill:            false,
		})
	}

	t.RenderJson(w, r, chart)
}
