package web

import (
	"github.com/gorilla/sessions"
	"gitlab.com/kendellfab/fazer"
)

type Handler struct {
	*fazer.Fazer
	*fazer.Manifest
	sessions.Store
}
