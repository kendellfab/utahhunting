package hunt

import (
	"strconv"

	"github.com/jinzhu/gorm"

	"gitlab.com/kendellfab/utahhunting/internal/platform/region"
)

const (
	GeneralEntry = "GeneralEntry"
	LimitedEntry = "LimitedEntry"
)

const (
	SpeciesDeer = "Deer"
	SpeciesElk  = "Elk"
)

type Hunt struct {
	gorm.Model
	HuntNumber         string `gorm:"unique_index:number_year_type_species"`
	HuntYear           int    `gorm:"index:hunt_year;unique_index:number_year_type_species"`
	EntryType          string `gorm:"index:entry_type"`
	HuntName           string
	RegionName         string `gorm:"index:region_name"`
	HuntType           string `gorm:"index:hunt_type;unique_index:number_year_type_species"`
	Species            string `gorm:"unique_index:number_year_type_species"`
	Permits            int
	HuntersAfield      int
	Harvest            int
	SuccessRate        float64
	HunterSatisfaction float64
}

func LoadGeneralEntry(data []string, huntYear int, species string) (Hunt, error) {
	h := Hunt{}
	h.HuntNumber = data[0]
	h.HuntYear = huntYear
	h.EntryType = GeneralEntry
	h.HuntName = data[1]
	h.RegionName = region.GetRegion(h.HuntName)
	h.HuntType = data[2]
	h.Species = species
	//h.UnitNumber = data[3]

	if permits, err := strconv.Atoi(data[4]); err == nil {
		h.Permits = permits
	}

	if hunters, err := strconv.Atoi(data[5]); err == nil {
		h.HuntersAfield = hunters
	}

	if harvest, err := strconv.Atoi(data[6]); err == nil {
		h.Harvest = harvest
	}

	if successRate, err := strconv.ParseFloat(data[7], 64); err == nil {
		h.SuccessRate = successRate
	}

	if hunterSatisfaction, err := strconv.ParseFloat(data[8], 64); err == nil {
		h.HunterSatisfaction = hunterSatisfaction
	}

	return h, nil
}

func LoadLimitedEntry(data []string, huntYear int, species string) (Hunt, error) {

	return Hunt{}, nil
}
