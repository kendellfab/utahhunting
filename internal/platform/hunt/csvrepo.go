package hunt

import (
	"encoding/csv"
	"io"
	"log"
	"sort"
)

type huntType int

const (
	typeName = iota
	typeType
)

type Name struct{}
type Type struct{}

type GeneralRepo struct {
	r           io.Reader
	huntYear    int
	entryType   string
	species     string
	hunts       []Hunt
	huntNameSet map[string]Name
	huntTypeSet map[string]Type
}

func InitRepo(r io.Reader, huntYear int, entryType string, species string) *GeneralRepo {
	gr := &GeneralRepo{r: r, huntYear: huntYear, entryType: entryType, species: species}
	gr.init()
	return gr
}

func (gr *GeneralRepo) init() {
	cr := csv.NewReader(gr.r)
	gr.huntNameSet = make(map[string]Name)
	gr.huntTypeSet = make(map[string]Type)

	for {
		records, err := cr.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal("error loading hunts", err)
		}

		switch gr.entryType {
		case GeneralEntry:
			if hunt, err := LoadGeneralEntry(records, gr.huntYear, gr.species); err == nil {
				gr.hunts = append(gr.hunts, hunt)
				gr.huntNameSet[hunt.HuntName] = Name{}
				gr.huntTypeSet[hunt.HuntType] = Type{}
			}
		case LimitedEntry:
			if hunt, err := LoadLimitedEntry(records, gr.huntYear, gr.species); err == nil {
				gr.hunts = append(gr.hunts, hunt)
				gr.huntNameSet[hunt.HuntName] = Name{}
				gr.huntTypeSet[hunt.HuntType] = Type{}
			}
		}
	}
}

func (gr *GeneralRepo) HuntsByTypeName() (map[string]map[string]Hunt, error) {
	results := make(map[string]map[string]Hunt)

	for _, h := range gr.hunts {
		if _, ok := results[h.HuntType]; !ok {
			results[h.HuntType] = make(map[string]Hunt)
		}
		results[h.HuntType][h.HuntName] = h
	}

	return results, nil
}

func (gr *GeneralRepo) HuntsByNameType() (map[string]map[string]Hunt, error) {
	results := make(map[string]map[string]Hunt)

	for _, h := range gr.hunts {
		if _, ok := results[h.HuntName]; !ok {
			results[h.HuntName] = make(map[string]Hunt)
		}
		results[h.HuntName][h.HuntType] = h
	}

	return results, nil
}

func (gr *GeneralRepo) HuntsNameSet() map[string]Name {
	return gr.huntNameSet
}

func (gr *GeneralRepo) HuntsNameSetRegion(region string) []string {
	huntNames := make(map[string]Name)

	for _, h := range gr.hunts {
		if h.RegionName == region {
			huntNames[h.HuntName] = Name{}
		}
	}

	var results []string
	for k, _ := range huntNames {
		results = append(results, k)
	}

	sort.Strings(results)
	return results
}

func (gr *GeneralRepo) HuntTypes() []string {
	var results []string
	for k, _ := range gr.huntTypeSet {
		results = append(results, k)
	}
	sort.Strings(results)
	return results
}

func (gr *GeneralRepo) ListHunts() []Hunt {
	return gr.hunts
}
