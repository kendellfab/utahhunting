package hunt

import "github.com/jinzhu/gorm"

type Manager struct {
	db *gorm.DB
}

func NewManager(db *gorm.DB) Manager {
	db.AutoMigrate(&Hunt{})

	m := Manager{db: db}
	return m
}

func (m Manager) StoreHunt(h *Hunt) error {
	err := m.db.Create(h).Error
	return err
}

func (m Manager) HuntsByTypeName(year int, region string, entry string, species string) (map[string]map[string]Hunt, error) {
	var hunts []Hunt
	err := m.db.Where(&Hunt{HuntYear: year, RegionName: region, EntryType: entry, Species: species}).Find(&hunts).Error
	if err != nil {
		return nil, err
	}

	results := make(map[string]map[string]Hunt)
	for _, h := range hunts {
		if _, ok := results[h.HuntType]; !ok {
			results[h.HuntType] = make(map[string]Hunt)
		}
		results[h.HuntType][h.HuntName] = h
	}
	return results, nil
}

func (m Manager) HuntsByType(year int, entry string, species string) (map[string]map[string]Hunt, error) {
	var hunts []Hunt

	err := m.db.Where(&Hunt{HuntYear: year, EntryType: entry, Species: species}).Find(&hunts).Error
	if err != nil {
		return nil, err
	}

	results := make(map[string]map[string]Hunt)
	for _, h := range hunts {
		if _, ok := results[h.HuntType]; !ok {
			results[h.HuntType] = make(map[string]Hunt)
		}
		results[h.HuntType][h.HuntName] = h
	}
	return results, nil
}

func (m Manager) HuntsNameSetInRegion(year int, region string, entry string, species string) ([]string, error) {
	var huntNames []string

	err := m.db.Model(&Hunt{}).Where(&Hunt{HuntYear: year, RegionName: region, EntryType: entry, Species: species}).Order("hunt_name").Pluck("distinct(hunt_name)", &huntNames).Error
	return huntNames, err
}

func (m Manager) HuntsNameSetYear(year int, entry string, species string) ([]string, error) {
	var huntNames []string

	err := m.db.Model(&Hunt{}).Where(&Hunt{HuntYear: year, EntryType: entry, Species: species}).Order("hunt_name").Pluck("distinct(hunt_name)", &huntNames).Error
	return huntNames, err
}

func (m Manager) HuntsTypeSetInRegion(year int, region string, entry string, species string) ([]string, error) {
	var huntTypes []string

	err := m.db.Model(&Hunt{}).Where(&Hunt{HuntYear: year, RegionName: region, EntryType: entry, Species: species}).Pluck("distinct(hunt_type)", &huntTypes).Error
	return huntTypes, err
}

func (m Manager) HuntsTypeSetYear(year int, entry string, species string) ([]string, error) {
	var huntTypes []string

	err := m.db.Model(&Hunt{}).Where(&Hunt{HuntYear: year, EntryType: entry, Species: species}).Pluck("distinct(hunt_type)", &huntTypes).Error
	return huntTypes, err
}

func (m Manager) UniqueSpecies() ([]string, error) {
	var species []string

	err := m.db.Model(&Hunt{}).Pluck("distinct(species)", &species).Error
	return species, err
}

func (m Manager) HuntYearsForSpecies(species string) ([]string, error) {
	var years []string

	err := m.db.Model(&Hunt{Species: species}).Pluck("distinct(hunt_year)", &years).Error
	return years, err
}

func (m Manager) UniqueEntryTypes() ([]string, error) {
	var entry []string

	err := m.db.Model(&Hunt{}).Pluck("distinct(entry_type)", &entry).Error
	return entry, err
}

func (m Manager) UniqueEntryTypesForSpecies(species string) ([]string, error) {
	var entry []string

	err := m.db.Model(&Hunt{Species: species}).Pluck("distinct(entry_type)", &entry).Error
	return entry, err
}
